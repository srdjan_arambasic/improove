$( document ).ready(function() {
	/////////////////////////
	// custom input number form net //
	/////////////////////////

		function calcQuantity() {
			var quantity=  $('.quantity-price-cart input').val(),
				$priceRegular = $('.js-regular'),
				currentPpriceRegular = $priceRegular.attr('data-price'),
				$pricePromo = $('.js-promo');
				currentPricePromo = $pricePromo.attr('data-price');

			$priceRegular.text((parseFloat(currentPpriceRegular) * quantity).toFixed(2))
			$pricePromo.text((parseFloat(currentPricePromo) * quantity).toFixed(2))
		}

		jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
		jQuery('.quantity').each(function() {
			var spinner = jQuery(this),
			input = spinner.find('input[type="number"]'),
			btnUp = spinner.find('.quantity-up'),
			btnDown = spinner.find('.quantity-down'),
			min = input.attr('min'),
			max = input.attr('max');

			btnUp.click(function() {
				var oldValue = parseFloat(input.val());
				if (oldValue >= max) {
					var newVal = oldValue;
				} else {
					var newVal = oldValue + 1;
				}
				spinner.find("input").val(newVal);
				spinner.find("input").trigger("change");

				calcQuantity();
			});

			btnDown.click(function() {
				var oldValue = parseFloat(input.val());
				if (oldValue <= min) {
					var newVal = oldValue;
				} else {
					var newVal = oldValue - 1;
				}
				spinner.find("input").val(newVal);
				spinner.find("input").trigger("change");


				calcQuantity();
			});
		});

	//////////////////////////////////////////////////////////////////
	// L AND XL PRODUCT VARIATIONS WITH BLACK COLOR ARE NOT ON SALE //
	//////////////////////////////////////////////////////////////////
		function checkPromoPrice() {
			var num = 0;

			$('.js-radio-wrapper-value input:checked').each(function () {
				var $this = $(this);

				if ($this[0].value == "L" || $this[0].value == "XL" || $this[0].value == "black" ) {
					num++;
				}
			});


			if (num >= 2 ) {
				$('.info-wrapper .price-wrap').removeClass('css-js-promo-price');
				$('.quantity-box .price').removeClass('css-js-promo-price');
			} else {
				$('.info-wrapper .price-wrap').addClass('css-js-promo-price');
				$('.quantity-box .price').addClass('css-js-promo-price');
			}

			num = 0;
		}


		$('.js-radio-wrapper-value input').on('click', function() {
			checkPromoPrice();
		});


		// to check on load
		checkPromoPrice();

	/////////////////////////////////////////
	// zoom image - https://codepen.io/ccrch/pen/yyaraz //
	/////////////////////////////////////////


		$('.js-img')
	    .on('mouseover', function(){
	    	$(this).children('.photo').css({'transform': 'scale('+ $(this).attr('data-scale') +')'});
	    })
	    .on('mouseout', function(){
	    	$(this).children('.photo').css({'transform': 'scale(1)'});
	    })
	    .on('mousemove', function(e){
	    	$(this).children('.photo').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'});
	    })
	    // tiles set up
	    .each(function(){
	    	$(this)
	        .append('<div class="photo"></div>')
	        .children('.photo').css({'background-image': 'url(assets/img/test.jpg'});
	    })
});